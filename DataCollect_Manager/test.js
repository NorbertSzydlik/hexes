const open = require('open');
var pt = require('promise-timeout');
 
var instancesOpen = 0;
var maxInstances = 3; 
 
 
function startBrowser() {
	if (instancesOpen >= maxInstances) {
		return;
	}
	instancesOpen += 1;
	
	if (instancesOpen < maxInstances) {
		setTimeout(function() {startBrowser();}, 1);
	}

	console.log("Start browser");
	
	var webdriver = require('selenium-webdriver'),
	  By = webdriver.By,
	  until = webdriver.until;

	var chrome = require('selenium-webdriver/chrome');

	var options = new chrome.Options();
	//options.setPreference('dom.allow_scripts_to_close_windows', true);
	//options.setPreference('browser.autofocus', false);
	options.addArguments('--window-position=0,0');
	options.addArguments('--window-size=1,1');
	options.addArguments('--disable-web-security');
	options.addArguments('--allow-file-access-from-files');
	options.addArguments('--user-data-dir=c:\\Projekty\\tmp\\data');
	
	//options.addArguments('-silent');
	//options.addArguments("-headless");

	var builder = new webdriver.Builder()
		.forBrowser('chrome')
		.setFirefoxOptions(options);
		
	var driver = builder.build();
	driver.get("file:///C:/Projekty/Hexes/Build_HTML/index.html");

	var checksPerformed = 0;

	function startNewBrowser() {
		console.log("close browser");
		instancesOpen -= 1;
		driver.quit().catch(function() {});
		startBrowser();
	}
	function check() {
		if (checksPerformed > 1300) {
			return startNewBrowser();
		}
		checksPerformed += 1;
		
		try {
			var windowHandles = driver.getAllWindowHandles();
			if (windowHandles == null) {
				startNewBrowser();
				return;
			}
			windowHandles.then(function() {
				setTimeout(function () {check();}, 100);
			}).catch(function(){
				startNewBrowser();
			});
		} catch(e) {
			startNewBrowser();
		}
	}
	setTimeout(function () {check();}, 10 * 1000);
}
startBrowser();
