﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface ITile : IEventSystemHandler
{
    void onColorChange(ActorColor color);
    void onClicked(MeshController controller);
    void onDestroyClusterCheck(out bool isDestructible);
    void onDestroyCluster();
}
