﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IMeshActor : IEventSystemHandler {
    void onTileBelowIsEmpty(out bool doStartFalling);
    void onTileBelowStartedFalling(out bool doStartFalling);
    void onTileBelowStillFalling(out bool doContinueFalling);
    void onBottomReached(out bool doFallThrough);
}
