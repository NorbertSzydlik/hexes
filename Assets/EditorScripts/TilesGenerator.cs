﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (MeshController), typeof(HexMesh))]
[ExecuteInEditMode]
public class TilesGenerator : MonoBehaviour {
    public GameObject prefab;
	// Use this for initialization
	void Start () {
        var meshController = GetComponent<MeshController>();
        var hexMesh = GetComponent<HexMesh>();
        hexMesh.RecalculateSizes();

        for (int y = 1; y < hexMesh.height + 1; y++)
        {
            for (int x = 0; x < hexMesh.width; x++)
            {
                var newTile = Instantiate(prefab);
                newTile.name = "Tile_" + x + "_" + y;
                newTile.transform.SetParent(transform);

                var newActor = newTile.GetComponent<MeshActor>();
                var position = HexMesh.OddQToCube(x, y);
                newActor.pos.x = (int)position.x;
                newActor.pos.y = (int)position.y;
                newActor.pos.z = (int)position.z;

                newTile.transform.position = hexMesh.calculateWorldPosition(HexMesh.OddQToCube(x, y));
                newActor.setTarget(newActor.transform.position);

                var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.SetParent(newTile.transform);
                sphere.transform.position = newTile.transform.position;
            }
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
