﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(HexMesh))]
[RequireComponent(typeof(RemoteJS))]
public class MeshController : MonoBehaviour {
    public float discardInputDistance = 10.0f;

    private HexMesh mesh;
    //private ClientInterpreter client;
    private PointsSubsystem pointsSubsystem;
    private MovesSubsystem movesSubsystem;
    private RandomGeneratorMono rng;
    private RemoteJS remote;

    private MeshActor[] actors;

    private List<MeshActor> fallingActors = new List<MeshActor>();
    private List<MeshActor> meshActorsToDelete = new List<MeshActor>();
    private MeshActor[,] grid;

    private Vector3[,] gridCenters;
    private int gridSize = 10;

    private bool initialized = false;
    private bool isSettled = true;
    private bool isMoveChecked = false;
    private bool isMovePossible = true;
    private int resetsCount = 0;

    float initialFallingSpeed = 1.0f;
    float fallingSpeed = 0.0f;
    float maxFallingSpeed = 20.0f;
    float acceleration = 15.0f;
    float whenLastSettled = 0.0f;
    public float REROLL_DELAY = 10.0f;
    public RandomNumberGeneratorType generatorType = RandomNumberGeneratorType.Xoshiro;

    public const int MIN_ACTORS_TO_DESTROY = 3;

    void Initialize()
    {
        mesh = GetComponent<HexMesh>();
        remote = GetComponent<RemoteJS>();
        //client = GetComponent<ClientInterpreter>();
        pointsSubsystem = GetComponent<PointsSubsystem>();
        movesSubsystem = GetComponent<MovesSubsystem>();
        actors = FindObjectsOfType<MeshActor>();
        rng = GetComponent<RandomGeneratorMono>();
        rng.SetrandomGenerator(remote.GetRandomGenerator());

        gridSize = Mathf.Max(mesh.width, mesh.height) + 1;

        CalculateGridCenters();
        FillGridWithExistingActors();

        rng.GetCurrentGenerator().SetSeed(29041991);

        foreach (MeshActor actor in actors)
        {
            var rngColor = actor.GetComponent<RandomizeColor>();
            if (rngColor != null)
            {
                rngColor.GenerateNewColor();
            }

            if (GetActorBelow(actor.pos.x, actor.pos.y, actor.pos.z) == null && HexMesh.CubeToOddQ(actor.pos.x, actor.pos.y - 1, actor.pos.z + 1).y < mesh.height)
            {
                TileBelowIsEmpty(actor);
            }
            
        }

        Application.runInBackground = true;

        initialized = true;
    }

    public bool isInitialized()
    {
        return initialized;
    }

    void CalculateGridCenters()
    {
        gridCenters = new Vector3[gridSize + 1, gridSize + 1];
        for(int x = 0; x < gridSize + 1; x++)
        {
            for(int y = 0; y < gridSize + 1; y++)
            {
                gridCenters[x, y] = mesh.calculateWorldPosition(HexMesh.OddQToCube(x, y));
            }
        }
    }
    void FillGridWithExistingActors()
    {
        grid = new MeshActor[gridSize, gridSize];
        foreach (MeshActor actor in actors)
        {
            actor.transform.position = GetCenterPositionAt(actor.pos.x, actor.pos.y, actor.pos.z);
            actor.setTarget(actor.transform.position);

            Debug.Assert(GetActorAt(actor.pos.x, actor.pos.y, actor.pos.z) == null, "There cannot be two tiles at the same position " + actor + " " + GetActorAt(actor.pos.x, actor.pos.y, actor.pos.z)  + " " + actor.pos.x + " " + actor.pos.y + " " + actor.pos.z);
            SetActorAt(actor, actor.pos.x, actor.pos.y, actor.pos.z);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(!initialized)
        {
            Initialize();
            return;
        }

        UpdateActors();
        CheckIfSettled();
        HandleInput();
        ComputeFalling();
        if (!isSettled)
        {
            isMoveChecked = false;
        }
        else if (!isMoveChecked)
        {
            CheckIfMovePossible();
            isMoveChecked = true;
        }
        if (!isMovePossible && Time.time - whenLastSettled > REROLL_DELAY)
        {
            pointsSubsystem.BoardReset();
            Reroll();
            resetsCount += 1;
        }
    }

    private Vector3 mouseDownPosition;
    private Vector3 mouseUpPosition;

    void UpdateActors() {
        if (fallingSpeed < initialFallingSpeed)
        {
            fallingSpeed = initialFallingSpeed;
        }
        fallingSpeed = Math.Min(maxFallingSpeed, fallingSpeed + fallingSpeed * acceleration * Time.deltaTime);
        

        var width = grid.GetLength(0);
        var height = grid.GetLength(1);

        for (var y = height - 1; y >= 0; --y)
        {
            for (var x = 0; x < width; ++x)
            {
                var actor = grid[x, y];
                if (actor == null)
                {
                    continue;
                }
                var actorBelow = GetActorBelow(actor.pos.x, actor.pos.y, actor.pos.z);
                var oldPosition = actor.transform.position;

                if (actorBelow != null && Vector3.Distance(actor.transform.position, actorBelow.transform.position) <= (mesh.hexHeight))
                {
                    actor.UpdateActor(0.0f);
                }
                else
                {
                    actor.UpdateActor(fallingSpeed);
                }
                if (actorBelow != null && Vector3.Distance(oldPosition, actorBelow.transform.position) <= (mesh.hexHeight))
                {
                    actor.transform.position = oldPosition;
                }
            }
        }
    }

    void HandleInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseDownPosition = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            mouseUpPosition = Input.mousePosition;

            HandleClick();
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            mouseDownPosition = Input.GetTouch(0).position;
        }
        else if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            mouseDownPosition = Input.GetTouch(0).position;

            HandleClick();
        }
    }

    void HandleClick()
    {
        if(Vector2.Distance(mouseDownPosition, mouseUpPosition) > discardInputDistance)
        {
            return;
        }

        var worldPosition = Camera.main.ScreenToWorldPoint(mouseUpPosition);

        var gridPosition = mesh.calculateGridPosition(worldPosition);

        var clickedActor = GetActorAt(gridPosition.x, gridPosition.y, gridPosition.z);
        HandleActorClicked(clickedActor);
    }

    public void HandleActorClicked(MeshActor clickedActor) {
        if (movesSubsystem.IsMovePossible() && clickedActor != null)
        {
            Debug.Log("CLICK!");
            ExecuteEvents.Execute<ITile>(clickedActor.gameObject, null, (target, eventData) => target.onClicked(this));
        }
    }

    void ComputeFalling()
    {
        fallingActors.RemoveAll(actor => actor == null);
        fallingActors.RemoveAll(actor => !actor.isFalling);

        foreach (var actor in grid)
        {
            if (actor == null)
            {
                continue;
            }
            var atBottom = HexMesh.CubeToOddQ(actor.pos.x, actor.pos.y, actor.pos.z).y >= mesh.height - 1;
            var actorBelow = GetActorBelow(actor.pos.x, actor.pos.y, actor.pos.z);
            if (!atBottom && actorBelow == null)
            {
                TileBelowIsEmpty(actor);
            }
        }

        foreach (MeshActor actor in fallingActors)
        {
            var actorBelow = GetActorBelow(actor.pos.x, actor.pos.y, actor.pos.z);
            var atBottom = HexMesh.CubeToOddQ(actor.pos.x, actor.pos.y, actor.pos.z).y >= mesh.height - 1;
            var reachedTarget = Vector3.Distance(actor.transform.position, actor.targetPosition) < 0.001f;

            if (actorBelow != null && !actorBelow.isFalling)
            {
                actor.targetPosition = GetCenterPositionAt(actor.pos.x, actor.pos.y, actor.pos.z);
                actor.isFalling = false;
            }
            if (reachedTarget)
            {
                if (atBottom)
                {
                    bool doFallTrough = true;
                    ExecuteEvents.Execute<IMeshActor>(actor.gameObject, null, (target, eventData) => target.onBottomReached(out doFallTrough));
                    if (!doFallTrough)
                    {
                        actor.isFalling = false;
                    }
                }
                else
                {
                    if (actorBelow == null)
                    {
                        SetActorAt(null, actor.pos.x, actor.pos.y, actor.pos.z);
                        actor.moveDown(1);
                        SetActorAt(actor, actor.pos.x, actor.pos.y, actor.pos.z);
                        actor.targetPosition = GetCenterPositionAt(actor.pos.x, actor.pos.y, actor.pos.z);
                    }
                }
            }
        }

        foreach (MeshActor actor in meshActorsToDelete)
        {
            DestroyMeshActor(actor);
        }
        meshActorsToDelete.RemoveAll((x) => true);
    }

    void CheckIfSettled()
    {
        if(fallingActors.Count > 0 && isSettled)
        {
            isSettled = false;
        }
        if(fallingActors.Count == 0 && !isSettled)
        {
            //client.NotifySettled();
            isSettled = true;
            fallingSpeed = 0.0f;

            whenLastSettled = Time.time;
        }
    }

    public void TileBelowIsEmpty(MeshActor actor)
    {
        if (actor == null)
        {
            return;
        }

        bool isFalling = false;
        ExecuteEvents.Execute<IMeshActor>(actor.gameObject, null, (target, eventData) => target.onTileBelowIsEmpty(out isFalling));
        if (isFalling)
        {
            ActorStartedFalling(actor);
        }
    }
    
    public void ActorStartedFalling(MeshActor actor)
    {
        if(actor == null)
        {
            return;
        }

        fallingActors.Add(actor);
        actor.targetPosition = GetCenterPositionAt(actor.pos.x, actor.pos.y - 1, actor.pos.z + 1);
        actor.isFalling = true;
        
        MeshActor actorAbove = GetActorAbove(actor.pos);
        TileBelowIsEmpty(actorAbove);
    }

    public void DestroyMeshActor(MeshActor actor)
    {
        fallingActors.Remove(actor);
        SetActorAt(null, actor.pos.x, actor.pos.y, actor.pos.z);

        var actorAbove = GetActorAbove(actor.pos);
        GameObject.Destroy(actor.gameObject);

        TileBelowIsEmpty(actorAbove);
    }

    Vector3 GetCenterPositionAt(Vector3 position)
    {
        return GetCenterPositionAt((int)position.x, (int)position.y, (int)position.z);
    }
    Vector3 GetCenterPositionAt(int x, int y, int z)
    {
        var pos = HexMesh.CubeToOddQ(x, y, z);
        return gridCenters[(int)pos.x, (int)pos.y];
    }

    public MeshActor GetActorAt(HexCubeCoordinate position)
    {
        return GetActorAt(position.x, position.y, position.z);
    }
    public MeshActor GetActorAt(Vector3 position)
    {
        return GetActorAt((int)position.x, (int)position.y, (int)position.z);
    }
    public MeshActor GetActorAt(int x, int y, int z)
    {
        var pos = HexMesh.CubeToOddQ(x, y, z);
        if(pos.x < 0 || pos.y < 0 || pos.x > mesh.width || pos.y > mesh.height)
        {
            return null;
        }
        return grid[(int)pos.x, (int)pos.y];
    }

    MeshActor GetActorAbove(HexCubeCoordinate position)
    {
        return GetActorAbove((int)position.x, (int)position.y, (int)position.z);
    }

    MeshActor GetActorAbove(Vector3 position)
    {
        return GetActorAbove((int)position.x, (int)position.y, (int)position.z);
    }

    MeshActor GetActorAbove(int x, int y, int z)
    {
        return GetActorAt(x, y + 1, z - 1);
    }

    MeshActor GetActorBelow(Vector3 position)
    {
        return GetActorBelow((int)position.x, (int)position.y, (int)position.z);
    }

    MeshActor GetActorBelow(int x, int y, int z)
    {
        return GetActorAt(x, y - 1, z + 1);
    }

    MeshActor GetNeighbour(HexCubeCoordinate of, int dx, int dy, int dz)
    {
        Debug.Assert(dx + dy + dz == 0, "Neighbour delta position should sum to zero");
        return GetActorAt(of.x + dx, of.y + dy, of.z + dz);
    }

    public void SetActorAt(MeshActor actor, Vector3 position)
    {
        SetActorAt(actor, (int)position.x, (int)position.y, (int)position.z);
    }

    public void AddFallingActor(MeshActor actor)
    {
        if (actor == null)
        {
            return;
        }
        actor.isFalling = true;
        fallingActors.Add(actor);
    }

    public void SetActorAt(MeshActor actor, int x, int y, int z)
    {
        if(z > mesh.height + 1)
        {
            meshActorsToDelete.Add(actor);
            return;
        }
        var pos = HexMesh.CubeToOddQ(x, y, z);
        grid[(int)pos.x, (int)pos.y] = actor;
    }

    public HashSet<MeshActor> GetClusterOfSameColour(MeshActor origin, MeshActor current, HashSet<MeshActor> actors)
    {
        if(current == null || origin.color != current.color)
        {
            return actors;
        }
        if(actors.Contains(current))
        {
            return actors;
        }

        actors.Add(current);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, 0, 1, -1), actors);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, 1, 0, -1), actors);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, 1, -1, 0), actors);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, 0, -1, 1), actors);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, -1, 0, 1), actors);
        actors = GetClusterOfSameColour(origin, GetNeighbour(current.pos, -1, 1, 0), actors);

        return actors;
    }
    public void TryToDestroyCluster(MeshActor origin)
    {
        if(origin == null || GetActorAt(origin.pos) != origin)
        {
            return;
        }
        var actors = new HashSet<MeshActor>();
        actors = GetClusterOfSameColour(origin, origin, actors);

        if(actors.Count < MIN_ACTORS_TO_DESTROY)
        {
            return;
        }

        var destructibleActors = new List<MeshActor>();

        foreach (var actor in actors)
        {
            bool isDestructible = false;
            ExecuteEvents.Execute<ITile>(actor.gameObject, null, (target, eventData) => target.onDestroyClusterCheck(out isDestructible));
            if(isDestructible)
            {
                destructibleActors.Add(actor);
            }
        }

        if(destructibleActors.Count < MIN_ACTORS_TO_DESTROY)
        {
            return;
        }

        pointsSubsystem.HexesDestroyed(destructibleActors.Count);
        movesSubsystem.HexesDestroyed();

        foreach (var actor in destructibleActors)
        {
            DestroyMeshActor(actor);
        }
    }
    public void TryToDestroyCluster(int x, int y, int z)
    {
        TryToDestroyCluster(GetActorAt(x, y, z));
    }

    public MeshActor[,] getGrid()
    {
        return grid;
    }

    void CheckIfMovePossible() {
        actors = FindObjectsOfType<MeshActor>();
        isMovePossible = false;
        foreach (var actor in actors) {
            var actorsCluster = new HashSet<MeshActor>();
            actorsCluster = GetClusterOfSameColour(actor, actor, actorsCluster);

            if (actorsCluster.Count >= MIN_ACTORS_TO_DESTROY)
            {
                isMovePossible = true;
                break;
            }
        }
    }

    public bool IsMovePossible()
    {
        return isMovePossible;
    }

    public bool IsSettled()
    {
        return isSettled;
    }

    void Reroll() {
        actors = FindObjectsOfType<MeshActor>();
        foreach (var actor in actors)
        {
            actor.SetColor((ActorColor)rng.GetCurrentGenerator().GetNext(1, 7));
        }
        CheckIfMovePossible();
        if(!IsMovePossible())
        {
            Reroll();
        }
    }
    public int GetResetsCount() { return resetsCount; }
}
