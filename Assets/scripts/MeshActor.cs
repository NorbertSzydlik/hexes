﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ActorColor
{
    Color_None,
    Color_Black,
    Color_White,
    Color_Yellow,
    Color_Green,
    Color_Red,
    Color_Blue
}

public class MeshActor : MonoBehaviour, IMeshActor {
    public bool isFalling;
    public HexCubeCoordinate pos;

    public ActorColor color;
    public void SetColor(ActorColor value)
    {
        color = value;
        ExecuteEvents.Execute<ITile>(gameObject, null, (target, eventData) => target.onColorChange(value));
    }

    private Vector3 _targetPosition;
    public Vector3 targetPosition
    {
        get
        {
            return _targetPosition;
        }
        set
        {
            _targetPosition = value;
            if(isCloseToTarget())
            {
                fallingSpeed = 0.0f;
                targetReached = true;
                transform.position = targetPosition;
            }
            else
            {
                targetReached = false;
            }
        }
    }
    float initialFallingSpeed = 1.0f;
    float fallingSpeed = 0.0f;
    float maxFallingSpeed = 50.0f;
    float acceleration = 15.0f;
    float epsilon = 0.00001f;

    bool targetReached = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void UpdateActor (float fallingSpeed) {
        if (!isFalling && targetReached) return;
        else if (isFalling && !targetReached) targetReached = false;
        
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * fallingSpeed);

        if(isCloseToTarget())
        {
            targetReached = true;
            transform.position = targetPosition;
        }
	}
    private bool isCloseToTarget()
    {
        return Mathf.Abs(transform.position.x - targetPosition.x) < epsilon && Mathf.Abs(transform.position.y - targetPosition.y) < epsilon;
    }
    public void setTarget(Vector3 position)
    {
        targetReached = false;
        targetPosition = position;
    }

    public void onTileBelowIsEmpty(out bool doStartFalling)
    {
        fallingSpeed = initialFallingSpeed;
        isFalling = true;
        doStartFalling = true;
    }
    public void onTileBelowStartedFalling(out bool doStartFalling)
    {
        fallingSpeed = initialFallingSpeed;
        isFalling = true;
        doStartFalling = true;
    }
    public void onBottomReached(out bool doFallThrough)
    {
        doFallThrough = false;
    }

    public void move(int offsetX, int offsetY, int offsetZ) {
        if(offsetX + offsetY + offsetZ == 0)
        {
            pos.x += offsetX;
            pos.y += offsetY;
            pos.z += offsetZ;
        }
    }

    public void moveDown(int offset) {
        move(0, -offset, offset);
    }

    public void onTileBelowStillFalling(out bool doContinueFalling)
    {
        doContinueFalling = true;
    }
}
