﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsSubsystem : MonoBehaviour
{
    float lastDestroyed = 0.0f;
    int points_ = 0;
    public int points {
        get { return points_; }
        set { points_ = value; text.text = points.ToString(); }
    }

    int minimalPoints = 10;
    int boardResetPenalty = 50;

    public UnityEngine.UI.Text text;
    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        lastDestroyed = Time.time - 120.0f;
        points = 0;
    }

    public void BoardReset() {
        points -= boardResetPenalty;
    }

    public void HexesDestroyed(int num) {
        points += CalculatePoints(num);

        Debug.Log("Points:");
        Debug.Log(points);

        lastDestroyed = Time.time;
    }

    int CalculatePoints(int num) {
        int pointsAward = minimalPoints;
        if (num > 3)
        {
            pointsAward += Mathf.RoundToInt(Mathf.LerpUnclamped(5.0f, 50.0f, (float)(num - 3) / 12.0f));
        }
        return pointsAward;
    }
}
