﻿/*using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;


using UnityEngine;
using System.Text.RegularExpressions;


public class Client : MonoBehaviour {
    public string host = "127.0.0.1";

    public int port = 27222;

    public bool socketReady = false;

    TcpClient socket;
    NetworkStream networkStream;
    StreamReader readerStream;
    StreamWriter writerStream;

    Thread readerThread;
    Thread writerThread;

    Queue<string> messages = new Queue<string>();
    Queue<string> messagesToSend = new Queue<string>();
    Semaphore _sem = new Semaphore(0, Int32.MaxValue);

    // Use this for initialization
    void Start () {
        try
        {
            socket = new TcpClient(host, port);
            networkStream = socket.GetStream();
            readerStream = new StreamReader(networkStream);
            writerStream = new StreamWriter(networkStream);

            readerThread = new Thread(readerLoop);
            readerThread.Start();

            writerThread = new Thread(writerLoop);
            writerThread.Start();

            socketReady = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error:" + e);
        }
    }

    void readerLoop()
    {
        Debug.Log("Started reading loop");
        string line;
        while ((line = readerStream.ReadLine()) != null)
        {
            Debug.Log("read line from server: " + line);
            messages.Enqueue(line);
        }
        Debug.Log("Finished reading loop");
    }

    void writerLoop()
    {
        Debug.Log("Started writing loop");
        while (true)
        {
            _sem.WaitOne();
            string msg;
            lock (messagesToSend)
            {
                msg = messagesToSend.Dequeue();
            }

            msg = msg.Trim();
            Debug.Log("Sending message: " + msg);
            writerStream.Write(msg + "\n");
            writerStream.Flush();
        }
    }

    public void sendMessage(string msg)
    {
        lock (messagesToSend)
        {
            Debug.Log("Enqueue message: " + msg);
            messagesToSend.Enqueue(msg);
        }
        _sem.Release();
    }

    bool isSocketReady()
    {
        return socketReady;
    }

    public bool isNewMessageWaiting()
    {
        return messages.Count != 0;
    }

    public string getMessage()
    {
        return messages.Dequeue();
    }

    void OnApplicationQuit()
    {
        if (!socketReady)
            return;
        writerStream.Close();
        readerStream.Close();
        socket.Close();
        socketReady = false;
    }
}
*/