﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

[RequireComponent(typeof(MeshController))]
[RequireComponent(typeof(MovesSubsystem))]
[RequireComponent(typeof(PointsSubsystem))]
[RequireComponent(typeof(RandomGeneratorMono))]
public class RemoteJS : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void NextMove(int[,] destroyableActors, int size);

    [DllImport("__Internal")]
    private static extern void EndGame(int points, int resets, int generator);

    [DllImport("__Internal")]
    private static extern int _GetRandomGenerator();


    [DllImport("__Internal")]
    private static extern void StartGame();

    [DllImport("__Internal")]
    private static extern int AutoplayGetX();

    [DllImport("__Internal")]
    private static extern int AutoplayGetY();

    [DllImport("__Internal")]
    private static extern int AutoplayGetZ();

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Remote JS start");
        //client = gameObject.GetComponent<Client>();

        controller = GetComponent<MeshController>();

        rng = GetComponent<RandomGeneratorMono>();

        movesSubsystem = GetComponent<MovesSubsystem>();

        pointsSubsystem = GetComponent<PointsSubsystem>();
        Debug.Log("Remote JS end");
    }

    void Update()
    {
        if (!jsAvailable)
        {
            return;
        }

        if (Time.time - lastTime < 0.25f)
        {
            return;
        }
        lastTime = Time.time;

        if (!controller.isInitialized())
        {
            Debug.Log("Not initialized");
            return;
        }

        if (!gameStarted)
        {
            StartGame();
            gameStarted = true;
        }

        if(!movesSubsystem.IsMovePossible())
        {
            if (!gameFinished)
            {
                EndGame(pointsSubsystem.points, controller.GetResetsCount(), Int32.Parse(rng.GetCurrentGeneratorType().ToString("d")));
            }
            gameFinished = true;
            return;
        }

        if(!controller.IsMovePossible())
        {
            Debug.Log("Controller Move not possible");
        }
        if (!controller.IsSettled())
        {
            Debug.Log("Not settled");
        }

        if (controller.IsMovePossible() && controller.IsSettled())
        {
            NotifySettled();
        }
    }

    public RandomNumberGeneratorType GetRandomGenerator() {
        if (jsAvailable)
        {
            var genNum = string.Format("{0:d}", _GetRandomGenerator());
            Debug.Log("GetRandomGenerator got: " + genNum);

            RandomNumberGeneratorType ret;
            if (Enum.TryParse(genNum, false, out ret))
            {
                return ret;
            }
        }
        Debug.Log("Returning default xoshiro");
        return RandomNumberGeneratorType.Xoshiro;
    }

    public void NotifySettled()
    {
        Debug.Log("Notify settled");
        var grid = controller.getGrid();
        var destroyableActors = new List<MeshActor>();
        
        for (int x = 0; x < grid.GetLength(0); x += 1)
        {
            for (int y = 0; y < grid.GetLength(1); y += 1)
            {
                var actor = grid[y, x];
                if (actor == null)
                {
                    continue;
                }

                var actors = new HashSet<MeshActor>();
                actors = controller.GetClusterOfSameColour(actor, actor, actors);

                if (actors.Count >= MeshController.MIN_ACTORS_TO_DESTROY)
                {
                    destroyableActors.Add(actor);
                }
            }
        }

        Debug.Log("Notify settled. destroyableActors=" + destroyableActors);

        if (destroyableActors.Count == 0)
        {
            Debug.Log("Destroyable actors not found");
            return;
        }

        var destroyableActorPositions = new int[destroyableActors.Count, 3];
        var i = 0;
        foreach(var actor in destroyableActors)
        {
            destroyableActorPositions[i, 0] = actor.pos.x;
            destroyableActorPositions[i, 1] = actor.pos.y;
            destroyableActorPositions[i, 2] = actor.pos.z;
            i += 1;
        }
        Debug.Log("Notify settled. destroyableActorPositions=" + arrToString(destroyableActorPositions));

        NextMove(destroyableActorPositions, destroyableActorPositions.GetLength(0));
        var posX = AutoplayGetX();
        var posY = AutoplayGetY();
        var posZ = AutoplayGetZ();

        controller.HandleActorClicked(controller.GetActorAt(posX, posY, posZ));
    }

    string arrToString(int[] arr)
    {
        var str = "[";
        for (var x = 0; x < arr.GetLength(0); ++x)
        {
            str += arr[x];
            str += ", ";
        }
        str += "]";
        return str;
    }
    string arrToString(int[,] arr)
    {
        var str = "[";
        for(var x = 0; x < arr.GetLength(0); ++x)
        {
            str += "[";
            for(var y = 0; y < arr.GetLength(1); ++y)
            {
                str += arr[x, y];
                str += ", ";
            }
            str += "],";
        }
        str += "]";
        return str;
    }

    //private Client client;
    private MeshController controller;
    private RandomGeneratorMono rng;
    private MovesSubsystem movesSubsystem;
    private PointsSubsystem pointsSubsystem;
    private RandomGeneratorMono generator;

    private bool gameStarted = false;
    private bool gameFinished = false;
    private float lastTime = 0.0f;

    public bool jsAvailable = true;
}
