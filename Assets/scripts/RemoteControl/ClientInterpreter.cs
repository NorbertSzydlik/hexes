﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshController), typeof(Client))]
public class ClientInterpreter : MonoBehaviour {

    private Client client;
    private MeshController controller;
    private RandomGeneratorMono rng;
    bool initialized = false;

	// Use this for initialization
	void Start () {
        client = gameObject.GetComponent<Client>();

        controller = GetComponent<MeshController>();

        rng = GetComponent<RandomGeneratorMono>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!initialized && controller.isInitialized() && rng.GetCurrentGenerator().IsSeedSet()) {
            Debug.Log("RUN");
            NotifySettled();
            initialized = true;
        }
        
		while (client.isNewMessageWaiting())
        {
            var msg = client.getMessage();
            Debug.Log("message: ");
            Debug.Log(msg);

            var tokens = msg.Split(null as string[], System.StringSplitOptions.RemoveEmptyEntries);
            switch(tokens[0])
            {
                case "click":
                    {
                        var x = System.Convert.ToInt32(tokens[1]);
                        var y = System.Convert.ToInt32(tokens[2]);
                        var z = System.Convert.ToInt32(tokens[3]);

                        controller.TryToDestroyCluster(x, y, z);
                    }
                    break;
                case "stop":
                    Application.Quit();
                    break;
                case "seed":
                    {
                        var seed = System.Convert.ToInt32(tokens[1]);
                        Debug.Log("set seed: " + seed);

                        rng.GetCurrentGenerator().SetSeed(seed);

                        var grid = controller.getGrid();
                        for (int x = 0; x < grid.GetLength(0); x += 1)
                        {
                            for (int y = 0; y < grid.GetLength(1); y += 1)
                            {
                                var actor = grid[y, x];

                                if (actor == null)
                                {
                                    continue;
                                }

                                var rc = actor.GetComponent<RandomizeColor>();
                                if (rc == null)
                                {
                                    continue;
                                }
                                rc.GenerateNewColor();
                            }
                        }
                    }
                    break;
            }
        }

    }
    
    public void NotifySettled()
    {
        var grid = controller.getGrid();
        var msg = @"{
    ""Command"": ""settled"",
    ""Map"": [";
        for (int x = 0; x < grid.GetLength(0); x += 1)
        {
            if (x > 0)
            {
                msg += ", ";
            }
            msg += "    [";
            for (int y = 0; y < grid.GetLength(1); y += 1)
            {
                if (y > 0)
                {
                    msg += ", ";
                }
                var actor = grid[y, x];

                if (actor == null)
                {
                    msg += "null";
                }
                else
                {
                    var actors = new HashSet<MeshActor>();
                    actors = controller.GetClusterOfSameColour(actor, actor, actors);
                    msg += "{";
                    msg += "\"Color\": \"" + actor.color + "\", ";
                    msg += "\"ClusterSize\": " + actors.Count + ", ";
                    msg += "\"X\": " + actor.pos.x + ", \"Y\": " + actor.pos.y + ", \"Z\": " + actor.pos.z;
                    msg += "}";
                }
            }
            msg += "]\n";
        }

        msg += "]\n}";
        Debug.Log("msg:");
        Debug.Log(msg);

        msg = msg.Replace('\n', ' ');

        client.sendMessage(msg);
    }
}
*/