﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SimpleHex : MonoBehaviour {
    Vector3[] hexVertices;
    Vector2[] hexUv;
    static int[] hexTriangles = new int[] {
        1, 2, 0,
        2, 3, 0,
        3, 4, 0,
        4, 5, 0,
        5, 6, 0,
        6, 1, 0
    };

    public float outerRadius;
    private float innerRadius;
    private Mesh mesh;

    private void Awake()
    {
        innerRadius = outerRadius * 0.866025404f;

        hexVertices = new Vector3[] {
            new Vector3(0f, 0f, 0f),
            new Vector3(0f, outerRadius, 0f),
            new Vector3(innerRadius, 0.5f * outerRadius, 0f),
            new Vector3(innerRadius, -0.5f * outerRadius, 0f),
            new Vector3(0f, -outerRadius, 0f),
            new Vector3(-innerRadius, -0.5f * outerRadius, 0f),
            new Vector3(-innerRadius, 0.5f * outerRadius, 0f)
        };

        hexUv = new Vector2[hexVertices.Length];
        for (int i = 0; i < hexUv.Length; i++)
        {
            hexUv[i] = new Vector2(hexVertices[i].x, hexVertices[i].z);
        }

        mesh = new Mesh();
        mesh.name = "Hexagon";

        mesh.vertices = hexVertices;
        mesh.triangles = hexTriangles;

        Graphics.DrawMeshNow(mesh, gameObject.transform.position, Quaternion.Euler(0f, 0f, 0f));

        GetComponent<MeshFilter>().mesh = mesh;
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update () {
        
    }
}
