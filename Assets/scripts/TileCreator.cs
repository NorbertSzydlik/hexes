﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCreator : MonoBehaviour
{
    public GameObject[] prefabs;
    public int x, y;

    private MeshController meshController;
    private HexMesh hexMesh;
    private RandomGeneratorMono rng;

    private GameObject getRandomPrefab()
    {
        return prefabs[rng.GetCurrentGenerator().GetNext(0, prefabs.Length)];
    }

    private void createRandomTile()
    {
        return;
        Debug.Log("createRandomTile");
        var prefab = getRandomPrefab();
        var newTile = Instantiate(prefab);
        
        newTile.name = "Tile_" + x + "_" + y + "_generated";
        newTile.transform.SetParent(transform.parent);

        var rngColor = newTile.GetComponent<RandomizeColor>();
        if (rngColor != null)
        {
            rngColor.GenerateNewColor(x, y, meshController);
        }

        var newActor = newTile.GetComponent<MeshActor>();
        newActor.isFalling = true;
        var position = HexMesh.OddQToCube(x, y);
        newActor.pos.x = (int)position.x;
        newActor.pos.y = (int)position.y;
        newActor.pos.z = (int)position.z;

        newTile.transform.position = hexMesh.calculateWorldPosition(position);
        newActor.setTarget(newTile.transform.position);

        meshController.SetActorAt(newActor, newActor.pos.x, newActor.pos.y, newActor.pos.z);
        meshController.AddFallingActor(newActor);
    }

    // Use this for initialization
    void Start () {
        meshController = GetComponentInParent<MeshController>();
        hexMesh = GetComponentInParent<HexMesh>();
        rng = GetComponentInParent<RandomGeneratorMono>();
    }
	
	// Update is called once per frame
	void Update () {
        if(!meshController.isInitialized())
        {
            return;
        }

        if ( meshController.GetActorAt(HexMesh.OddQToCube(x, y + 1)) == null && meshController.GetActorAt(HexMesh.OddQToCube(x, y)) == null)
        {
            Debug.Log("Actor does not exist");
            createRandomTile();
        }
	}
}
