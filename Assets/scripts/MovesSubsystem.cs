﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovesSubsystem : MonoBehaviour
{
    public UnityEngine.UI.Text text;
    int availableMoves;
    public int availableMovesAfterReset = 60;

    void Start()
    {
        Reset();
        
    }

    void Reset()
    {
        availableMoves = availableMovesAfterReset;
        text.text = availableMoves.ToString();
    }
    
    public bool IsMovePossible()
    {
        return availableMoves > 0;
    }

    public void HexesDestroyed()
    {
        --availableMoves;

        text.text = availableMoves.ToString();
    }
}
