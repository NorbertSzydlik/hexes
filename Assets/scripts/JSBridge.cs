﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class JSBridge : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void Hello();

    [DllImport("__Internal")]
    private static extern void HelloString(string str);
 
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("JSBridge start");
        Hello();

        HelloString("This is a string.");

        Debug.Log("JSBridge end");
    }
}
