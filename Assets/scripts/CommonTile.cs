﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommonTile : MonoBehaviour, ITile {
    MeshActor meshActor;
    // Use this for initialization
    void Start () {
        InstantiateTile();
    }

    public void InstantiateTile()
    {
        var children = new List<GameObject>();
        foreach (Transform child in transform) children.Add(child.gameObject);
        children.ForEach(child => Destroy(child));

        meshActor = GetComponentInParent<MeshActor>();
        GameObject newTile;
        switch (meshActor.color)
        {
            case ActorColor.Color_Black:
                newTile = Instantiate(Resources.Load("prefabs/Tile_black") as GameObject, transform);
                break;
            case ActorColor.Color_Blue:
                newTile = Instantiate(Resources.Load("prefabs/Tile_blue") as GameObject, transform);
                break;
            case ActorColor.Color_Green:
                newTile = Instantiate(Resources.Load("prefabs/Tile_green") as GameObject, transform);
                break;
            case ActorColor.Color_Red:
                newTile = Instantiate(Resources.Load("prefabs/Tile_red") as GameObject, transform);
                break;
            case ActorColor.Color_White:
                newTile = Instantiate(Resources.Load("prefabs/Tile_white") as GameObject, transform);
                break;
            case ActorColor.Color_Yellow:
                newTile = Instantiate(Resources.Load("prefabs/Tile_yellow") as GameObject, transform);
                break;
            default:
                newTile = null;
                break;
        }
        if (newTile != null)
        {
            newTile.transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    public void onColorChange(ActorColor color)
    {
        InstantiateTile();
    }

    public void onClicked(MeshController controller)
    {
        controller.TryToDestroyCluster(meshActor);
    }

    public void onDestroyClusterCheck(out bool isDestructible)
    {
        isDestructible = true;
    }

    public void onDestroyCluster()
    {
    }
}
