﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RandomNumberGeneratorType
{
    System = 1,
    Xoshiro = 2,
    CheckingWeak = 3,
    CheckingStrong = 4
}
