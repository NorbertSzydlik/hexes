﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGeneratorMono : MonoBehaviour
{
    private void Start()
    {
        SetrandomGenerator(m_generatorType);
    }

    private RandomNumberGeneratorType m_generatorType = RandomNumberGeneratorType.System;

    public void SetrandomGenerator(RandomNumberGeneratorType type)
    {
        m_generatorType = type;
        switch (m_generatorType)
        {
            case RandomNumberGeneratorType.Xoshiro:
                m_currentGenerator = new RandomGenerator_System();
                break;
            case RandomNumberGeneratorType.CheckingWeak:
                m_currentGenerator = new RandomGenerator_checkingWeak();
                break;
            case RandomNumberGeneratorType.CheckingStrong:
                m_currentGenerator = new RandomGenerator_checkingStrong();
                break;
            case RandomNumberGeneratorType.System:
            default:
                m_currentGenerator = new RandomGenerator_System();
                break;
        }
        m_currentGenerator.Initialize();
    }
    public IRandomGenerator GetCurrentGenerator()
    {
        return m_currentGenerator;
    }

    public RandomNumberGeneratorType GetCurrentGeneratorType()
    {
        return m_generatorType;
    }

    private IRandomGenerator m_currentGenerator;
}
