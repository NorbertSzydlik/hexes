﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGenerator_checkingStrong : IRandomGenerator
{
    RandomGenerator_xoshiro rng = new RandomGenerator_xoshiro();

    public int GetNext()
    {
        return rng.GetNext();
    }

    public int GetNext(int min, int max)
    {
        var result = rng.GetNext(min, max);
        Debug.Log("CheckingStrong. GetNext: " + result + ", min=" + min + ", max=" + max);
        return result;
    }

    public int GetNext(int min, int max, int x, int y, MeshController meshController)
    {
        MeshActor actor = null;
        int notEmptyY = y + 1;
        while (actor == null && notEmptyY < meshController.getGrid().GetLength(1))
        {
            actor = meshController.GetActorAt(HexMesh.OddQToCube(x, notEmptyY));
            ++notEmptyY;
        }

        Debug.Log("strong. actor=" + actor);
        if (actor == null)
        {
            return GetNext(min, max);
        }

        var actorsToCheck = new MeshActor[5];
        actorsToCheck[0] = actor;
        actorsToCheck[1] = meshController.GetActorAt(HexMesh.OddQToCube(x - 1, notEmptyY));
        actorsToCheck[2] = meshController.GetActorAt(HexMesh.OddQToCube(x + 1, notEmptyY));
        actorsToCheck[3] = meshController.GetActorAt(HexMesh.OddQToCube(x - 1, notEmptyY + 1));
        actorsToCheck[4] = meshController.GetActorAt(HexMesh.OddQToCube(x + 1, notEmptyY + 1));

        System.Func<int, bool> atLeastOneSame = (int color) => {
            bool result = false;
            result |= actorsToCheck[0] != null && (int)actorsToCheck[0].color == color;
            result |= actorsToCheck[1] != null && (int)actorsToCheck[1].color == color;
            result |= actorsToCheck[2] != null && (int)actorsToCheck[2].color == color;
            result |= actorsToCheck[3] != null && (int)actorsToCheck[3].color == color;
            result |= actorsToCheck[4] != null && (int)actorsToCheck[4].color == color;
            return result;
        };

        var newColor = GetNext(min, max);
        while(atLeastOneSame(newColor))
        {
            newColor = GetNext(min, max);
        }
        return newColor;
    }

    public void Initialize()
    {
        rng.Initialize();
    }

    public bool IsSeedSet()
    {
        return rng.IsSeedSet();
    }

    public void SetSeed(int seed)
    {
        rng.SetSeed(seed);
    }
}
