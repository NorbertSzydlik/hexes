﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeColor : MonoBehaviour {
    private RandomGeneratorMono rng;
    const int MinColor = 1;
    const int MaxColor = 7;

    void initRng()
    {
        if (rng == null)
        {
            rng = GetComponentInParent<RandomGeneratorMono>();
        }

        Debug.Assert(rng != null, "RandomGeneratorMono should be attached");
    }

    public void GenerateNewColor()
    {
        initRng();

        SetColorNum(rng.GetCurrentGenerator().GetNext(MinColor, MaxColor));
    }

    public void GenerateNewColor(int x, int y, MeshController meshController)
    {
        initRng();

        SetColorNum(rng.GetCurrentGenerator().GetNext(MinColor, MaxColor, x, y, meshController));
    }

    void SetColorNum(int colorNum) {
        var meshActor = GetComponentInParent<MeshActor>();
        Debug.Assert(meshActor != null, "You can only use RandomizeColor on MeshActor instance");
        
        meshActor.SetColor((ActorColor)colorNum);
    }
}
