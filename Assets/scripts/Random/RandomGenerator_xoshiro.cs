﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class RandomGenerator_xoshiro : IRandomGenerator
{   
    public int GetNext()
    {
        uint result_startstar = rotl(s[0] * 5, 7) * 9;
        uint t = s[1] << 9;

        s[2] ^= s[0];
        s[3] ^= s[1];
        s[1] ^= s[2];
        s[0] ^= s[3];

        s[2] ^= t;

        s[3] = rotl(s[3], 11);

        return (int)result_startstar;
    }

    public int GetNext(int min, int max)
    {
        uint r = (uint)GetNext();
        float f = (float)r / uint.MaxValue;

        int m1 = Math.Min(min, max);
        int m2 = Math.Max(min, max);

        var result = Mathf.FloorToInt(f * (float)(m2 - m1)) + m1;
        Debug.Log("GetNext, r=" + r + ", f=" + f + ", m1=" + m1 + ", m2=" + m2 + ", result=" + result);

        return result;
    }

    public int GetNext(int min, int max, int x, int y, MeshController meshController)
    {
        return GetNext(min, max);
    }

    public void Initialize()
    {
        var previousSeedSet = m_isSeedSet;
        var rng = new System.Random();
        SetSeed(rng.Next());
        m_isSeedSet = previousSeedSet;
    }
    
    public bool IsSeedSet()
    {
        return m_isSeedSet;
    }

    public void SetSeed(int seed)
    {
        s[0] = (uint)seed;
        s[1] = (uint)seed;
        s[2] = (uint)seed;
        s[3] = (uint)seed;
        jump();

        m_isSeedSet = true;
    }
   

    private void jump()
    {
        uint[] JUMP = { 0x8764000b, 0xf542d2d3, 0x6fa035c3, 0x77f2db5b };

        uint s0 = 0;
        uint s1 = 0;
        uint s2 = 0;
        uint s3 = 0;
        for (int i = 0; i < 4; i++)
		for (int b = 0; b < 32; b++)
        {
            if ((JUMP[i] & 1 << b) != 0)
            {
                s0 ^= s[0];
                s1 ^= s[1];
                s2 ^= s[2];
                s3 ^= s[3];
            }
            GetNext();
        }

        s[0] = s0;
        s[1] = s1;
        s[2] = s2;
        s[3] = s3;
    }

    private uint rotl(uint x, int k)
    {
        return (x << k) | (x >> (32 - k));
    }

    private bool m_isSeedSet = false;
    private uint[] s = { 0, 0, 0, 0 };
}
