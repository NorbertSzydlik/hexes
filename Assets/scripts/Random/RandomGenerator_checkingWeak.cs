﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGenerator_checkingWeak : IRandomGenerator
{
    RandomGenerator_xoshiro rng = new RandomGenerator_xoshiro();

    public int GetNext()
    {
        return rng.GetNext();
    }

    public int GetNext(int min, int max)
    {
        var result = rng.GetNext(min, max);
        Debug.Log("CheckingWeak. GetNext: " + result + ", min=" + min + ", max=" + max);
        return result;
    }

    public int GetNext(int min, int max, int x, int y, MeshController meshController)
    {
        MeshActor actor = null;
        int notEmptyY = y + 1;
        while (actor == null && notEmptyY < meshController.getGrid().GetLength(1))
        {
            actor = meshController.GetActorAt(HexMesh.OddQToCube(x, notEmptyY));
            ++notEmptyY;
        }

        Debug.Log("weak. actor=" + actor);
        if (actor == null)
        {
            return GetNext(min, max);
        }

        var actorColor = (int)actor.color;
        var newColor = GetNext(min, max);
        while(newColor == actorColor)
        {
            newColor = GetNext(min, max);
        }
        return newColor;
    }

    public void Initialize()
    {
        rng.Initialize();
    }

    public bool IsSeedSet()
    {
        return rng.IsSeedSet();
    }

    public void SetSeed(int seed)
    {
        rng.SetSeed(seed);
    }
}
