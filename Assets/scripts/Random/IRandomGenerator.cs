﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRandomGenerator
{
    void Initialize();
    void SetSeed(int seed);
    int GetNext();
    int GetNext(int min, int max);
    int GetNext(int min, int max, int x, int y, MeshController meshController);

    bool IsSeedSet();
}
