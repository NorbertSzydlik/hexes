﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomGenerator_System : IRandomGenerator {
    private static System.Random rng = null;
    private static bool seedSet = false;

    public void Initialize()
    {
        rng = new System.Random();
    }
    public void SetSeed(int seed)
    {
        Debug.Log("seed is set: " + seed);
        rng = new System.Random(seed);
        seedSet = true;
    }
    public int GetNext()
    {
        if (rng == null) Initialize();
        return rng.Next();
    }
    public int GetNext(int min, int max)
    {
        if (rng == null) Initialize();
        return rng.Next(min, max);
    }

    public bool IsSeedSet()
    {
        return seedSet;
    }

    public int GetNext(int min, int max, int x, int y, MeshController meshController)
    {
        return GetNext(min, max);
    }
}
