﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexMesh : MonoBehaviour {
    public GameObject hex;
    public float hexSize;

    public int width;
    public int height;
    public float gap;

    public float hexHeight;
    float hexWidth;

    Vector3 startPosition;

    float outerRadius;
    float innerRadius;

    // Use this for initialization
    void Start () {
        RecalculateSizes();

        drawMesh();
	}

    public void RecalculateSizes()
    {
        hexWidth = (hexSize + gap) * 2.0f;
        hexHeight = (Mathf.Sqrt(3) / 2) * hexWidth + gap;

        startPosition.x = transform.position.x + (hexWidth / 2f);
        startPosition.y = transform.position.y - (hexHeight / 2f);

        outerRadius = hexWidth / 2f;
        innerRadius = outerRadius * 0.866025404f;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public Vector3 calculateWorldPosition(HexCubeCoordinate gridPosition)
    {
        var position = new Vector3();
        position.x = startPosition.x + (gridPosition.x * (outerRadius * 1.5f));
        position.y = startPosition.y + (gridPosition.y * hexHeight / 2.0f - gridPosition.z * hexHeight / 2.0f);
        position.z = 0f;

        return position;
    }

    public HexCubeCoordinate calculateGridPosition(Vector3 worldPosition)
    {
        var localPosition = transform.InverseTransformPoint(worldPosition);

        var tX = localPosition.x / (outerRadius * SQRT3) - outerRadius / (SQRT3 * 1.5f);
        var tY = localPosition.y / (outerRadius * SQRT3) + outerRadius / 3f;

        var temp = Mathf.Floor(tY + SQRT3 * tX + 1);
        var q = Mathf.Floor( (temp + Mathf.Floor(-tY + SQRT3 * tX + 1)) / 3 );
        var r = -Mathf.Floor( (Mathf.Floor(2 * tY + 1) + temp) / 3 );

        var x = q;
        var y = -q - r;
        var z = r;

        var iX = (int)x;
        var iY = (int)y;
        var iZ = (int)z;

        if (iX + iY + iZ != 0)
        {
            float x_diff = Mathf.Abs((float)iX - x);
            float y_diff = Mathf.Abs((float)iY - y);
            float z_diff = Mathf.Abs((float)iZ - z);

            if (x_diff > y_diff && x_diff > z_diff)
                iX = -iY - iZ;
            else if (y_diff > z_diff)
                iY = -iX - iZ;
            else
                iZ = -iX - iY;
        }

        Debug.Assert(iX + iY + iZ == 0, "Cube coordinates does not sum to zero");
        var calculatedPosition = new HexCubeCoordinate(iX, iY, iZ);
        return calculatedPosition;
    }

    void drawMesh()
    {
        for (int row = 1; row < height; ++row)
            for (int col = 0; col < width; ++col)
            {
                GameObject currentHex = Instantiate(hex);
                currentHex.transform.position = calculateWorldPosition(HexCubeCoordinate.FromOddQ(col, row));
                currentHex.transform.localScale = new Vector3(hexSize, hexSize, 1.0f);
                currentHex.transform.name = "HexBg_" + col + "_" + row;

                currentHex.transform.parent = this.transform;
            }
    }

    public static Vector2 CubeToOddQ(HexCubeCoordinate cubePosition)
    {
        return CubeToOddQ((int)cubePosition.x, (int)cubePosition.y, (int)cubePosition.z);
    }
    public static Vector2 CubeToOddQ(int x, int y, int z)
    {
        return new Vector2(x, z + (x - ((int)x & 1)) / 2);
    }

    public static HexCubeCoordinate OddQToCube(Vector2 oddQPosition)
    {
        return OddQToCube((int)oddQPosition.x, (int)oddQPosition.y);
    }

    public static HexCubeCoordinate OddQToCube(int col, int row)
    {
        var cubePosition = new HexCubeCoordinate(
            col,
            0,
            row - (col - (col % 2)) / 2
        );
        cubePosition.y = -cubePosition.x - cubePosition.z;
        return cubePosition;
    }

    public readonly float SQRT3 = Mathf.Sqrt(3f);
}
