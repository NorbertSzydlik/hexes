﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HexAxialCoordinate : System.Object {
    public int q;
    public int r;

    public HexAxialCoordinate() : this(0, 0)
    {
    }
    public HexAxialCoordinate(int q, int r)
    {
        this.q = q;
        this.r = r;
    }

    public static HexAxialCoordinate FromOddQ(int q, int r)
    {
        return new HexAxialCoordinate(q, r - (q - (q & 1)) / 2);
    }

    public HexCubeCoordinate GetCubeCoordinates()
    {
        return new HexCubeCoordinate(q, -q -r, r);
    }

    public Vector3 GetVector()
    {
        return new Vector3(q, r, 0);
    }
}
