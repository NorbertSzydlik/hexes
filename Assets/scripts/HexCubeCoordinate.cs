﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HexCubeCoordinate : System.Object {
    public int x;
    public int y;
    public int z;

    public HexCubeCoordinate() : this(0, 0, 0)
    {
    }

    public HexCubeCoordinate(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public HexAxialCoordinate GetAxialCoordinates()
    {
        return new HexAxialCoordinate(x, z);
    }

    public static HexCubeCoordinate FromOddQ(int q, int r)
    {
        var x = q;
        var z = r -(q - (q & 1)) / 2;
        var y = -x - z;
        return new HexCubeCoordinate(x, y, z);
    }

    override public string ToString() 
    {
        return "cube[" + x + ", " + y + ", " + z + "]";
    }
}
