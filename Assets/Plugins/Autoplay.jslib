var Autoplay = (function() {
    var x = 0, y = 0, z = 0;
    return {
        NextMove: function (positions, size) {
            var positionsJs = [];
            for(var i = 0; i < size; i++)
            {
                positionsJs[i] = [];
                positionsJs[i][0] = HEAP32[(positions >> 2) + i + 0];
                positionsJs[i][1] = HEAP32[(positions >> 2) + i + 1];
                positionsJs[i][2] = HEAP32[(positions >> 2) + i + 2];
            }

            console.log("positionsJs=", JSON.stringify(positionsJs));
            if (positionsJs == null || positionsJs[0] == null) {
                console.log("Positions is null/undefined!");
                return;
            }

            var pos = Math.floor(Math.random() * (size-1));  

            var positionsToReturn = [positionsJs[pos][0], positionsJs[pos][1], positionsJs[pos][2]];

            x = positionsToReturn[0];
            y = positionsToReturn[1];
            z = positionsToReturn[2];
        },

        EndGame: function (points, resets, generator) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'http://localhost:9090/data', false);

            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xhr.send(JSON.stringify({points: points, resets: resets, generator: generator}));

            window.close();
        },
        _GetRandomGenerator: function() {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://localhost:9090/rng', false);
            xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

            xhr.send();

            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                console.log(xhr.response);
                if (!xhr.response) {
                     window.close();
                }
                var response = JSON.parse(xhr.response);
                if (response) {
                    return response.rng || 1;
                }
                return 1;
            } else if (xhr.readyState === XMLHttpRequest.DONE) {
                console.log("ERROR status:", xhr.status);
                return 1;
            } else {
                window.close();
                return 1;
            }

            return 1;
        },
        StartGame: function() {
            console.log("StartGame");
        },

        AutoplayGetX: function() {
            return x;
        },

        AutoplayGetY: function() {
            return y;
        },
        
        AutoplayGetZ: function() {
            return z;
        }
    };
})();

mergeInto(LibraryManager.library, Autoplay);
