public HexCubeCoordinate calculateGridPosition(Vector3 worldPosition)
{
    var localPosition = transform.InverseTransformPoint(worldPosition);

    var tX = 
        localPosition.x / (outerRadius * SQRT3) - outerRadius / (SQRT3 * 1.5f);
    var tY = localPosition.y / (outerRadius * SQRT3) + outerRadius / 3f;

    var temp = Mathf.Floor(tY + SQRT3 * tX + 1);
    var q = Mathf.Floor( (temp + Mathf.Floor(-tY + SQRT3 * tX + 1)) / 3 );
    var r = -Mathf.Floor( (Mathf.Floor(2 * tY + 1) + temp) / 3 );

    var x = q;
    var y = -q - r;
    var z = r;

    var iX = (int)x;
    var iY = (int)y;
    var iZ = (int)z;

    if (iX + iY + iZ != 0)
    {
        float x_diff = Mathf.Abs((float)iX - x);
        float y_diff = Mathf.Abs((float)iY - y);
        float z_diff = Mathf.Abs((float)iZ - z);

        if (x_diff > y_diff && x_diff > z_diff)
            iX = -iY - iZ;
        else if (y_diff > z_diff)
            iY = -iX - iZ;
        else
            iZ = -iX - iY;
    }

    var calculatedPosition = new HexCubeCoordinate(iX, iY, iZ);
    return calculatedPosition;
}