const testFolder = '../DataCollect/collectedData/';
const fs = require('fs');
const _ = require('lodash');

var allData = {};

console.log("Collecting");
fs.readdirSync(testFolder).forEach(file => {
  var data = fs.readFileSync(testFolder + file, {
    encoding: 'utf8'
  });
  data = JSON.parse(data);
  allData[data.generator] = allData[data.generator] || {points: {}, resets: {}};
  allData[data.generator]['points'][data.points] = allData[data.generator]['points'][data.points] + 1 || 1;
  allData[data.generator]['resets'][data.resets] = allData[data.generator]['resets'][data.resets] + 1 || 1;
});


console.log("saving");
fs.writeFileSync('result.csv', '');
_.forEach(allData, function(data, generator) {
  var dataToSave = _.map(data['points'], (num, points) => generator + ',' + points + ',' + num).join('\n') + '\n';
  fs.writeFileSync('result.csv', dataToSave, {flag: 'a'});
});

console.log("DONE");