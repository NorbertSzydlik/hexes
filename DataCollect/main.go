package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	http.HandleFunc("/data", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Add("Access-Control-Allow-Methods", "POST")
			w.Header().Add("Access-Control-Allow-Credentials", "true")
			w.Header().Add("Access-Control-Max-Age ", "3600")
			w.Header().Add("Access-Control-Allow-Origin", "*")
			w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
			w.Header().Add("Allow", "GET, HEAD, POST, TRACE")
			w.Header().Add("Content-Type", "text/plain; charset=utf-8")
		} else if r.Method == "POST" {
			t := time.Now()
			fileName := fmt.Sprintf("collectedData/%s.json", t.Format("./2006-01-02_15-04-05.999999999"))

			requestBytes, err := ioutil.ReadAll(r.Body)
			defer r.Body.Close()
			check(err)

			file, err := os.Create(fileName)
			check(err)
			defer file.Close()

			file.Write(requestBytes)

			fmt.Printf("Saved '%s' in '%s'\n", requestBytes, fileName)

			w.Header().Add("Access-Control-Allow-Origin", "*")
			w.WriteHeader(200)
			fmt.Fprintf(w, "{\"status\": \"OK\"}")
		}
	})

	nextRng := make(chan int, 1)

	go func() {
		const rngModulo = 4
		currRng := 1
		for {
			nextRng <- currRng
			currRng = (currRng + 1) % rngModulo
		}
	}()

	http.HandleFunc("/rng", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Add("Access-Control-Allow-Methods", "POST")
			w.Header().Add("Access-Control-Allow-Credentials", "true")
			w.Header().Add("Access-Control-Max-Age ", "3600")
			w.Header().Add("Access-Control-Allow-Origin", "*")
			w.Header().Add("Access-Control-Allow-Headers", "Content-Type")
			w.Header().Add("Allow", "GET, HEAD, POST, TRACE")
			w.Header().Add("Content-Type", "text/plain; charset=utf-8")
		} else if r.Method == "GET" {
			w.Header().Add("Access-Control-Allow-Origin", "*")

			w.WriteHeader(200)
			fmt.Fprintf(w, "{\"rng\": %d}", <-nextRng)
		}
	})

	log.Println("Starting server")
	log.Fatal(http.ListenAndServe(":9090", nil))
}

func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
